"""Exceptions"""


class AFIError(Exception):
    """Base exception"""


class UnknownType(AFIError):
    """Raised when attempting to process unknown type"""


class InvalidCommand(AFIError):
    """Raised when trying to invoke invalid command"""
