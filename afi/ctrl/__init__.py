import logging

__all__ = [
    'vel', 'status', 'profibus', 'homing', 'config', 'capture',
    'manual', 'can', 'ptp', 'commands', 'rs485', 'motion',
    'progio0', 'errmem0', 'gear', 'io', 'control', 'settings'
]


logger = logging.getLogger('afi')


CODE_ROOT = 0x3000


class Command:
    """Base class for Lexium ILA1F commands.

    When inherited and all attributes are set
    simply ``write_to_node(node, value)`` or
    ``read_from_node(node)``
    """
    command_index = NotImplemented
    command_name = NotImplemented
    canopen_type = NotImplemented
    valid_range = NotImplemented
    unit = NotImplemented
    factory_value = NotImplemented
    writable = False
    persistent = False

    def __init__(self, node):
        self.node = node

    def __str__(self):
        name = self.command_name
        idx, sub = self._cmd_idx
        return '{0} - {1}:{2} (0x{1:02X}:0x{2:02X})'.format(name, idx, sub)

    def __repr__(self):
        return str(self)

    @property
    def _cmd_idx(self):
        """Returns (idx, subidx) of a command"""
        idx, sub = self.command_index
        return (CODE_ROOT + idx, sub)

    def write(self, value):
        """Writes ``value`` to node"""
        msg = self.canopen_type(value)
        logger.debug(
            'Attempt to write {} to {} on node {}'.format(
                msg, self._cmd_idx, self.node.id))
        ret = self.node.sdo.download(*self._cmd_idx, data=msg.as_canopen)
        return self.canopen_type(ret)

    def read(self):
        """Returns response from node for current command"""
        logger.debug(
            'Attempt to read {} from node {}'.format(
                self._cmd_idx, self.node.id))
        ret = self.node.sdo.upload(*self._cmd_idx)
        return self.canopen_type(ret)

    @classmethod
    def write_to_node(cls, node, value):
        """Writes ``value`` to ``node`` and returns it's response"""
        return cls(node).write(value)

    @classmethod
    def read_from_node(cls, node):
        """Returns value from ``node``"""
        return cls(node).read()
