"""Module autogenerated, may contain errors
Use at your own risk
"""
from afi import data_types
from . import Command


class StartMan(Command):
    """
    Starting a jog
    Assign ment of bits:
    Bit 0: Clockwise direction of rotation
    Bit 1: Counterclockwise direction of rotation
    Bit 2: 0 = slow 1 = fast
    Bit 3: Automatic processing of power stage
    If bit 3 is set to 1, a jog movement can be started even if the
    power stage is switched off: If the drive is in state 4 (Ready-
    ToSwitchOn), the power stage is automatically switched on
    when the jog movement is started and switched off when the
    movement is finished.

    """
    command_index = (0x29, 0x01)
    command_name = 'Manual.startMan'
    canopen_type = data_types.UINT16
    valid_range = range(0, 16)
    factory_value = 0
    writable = True


class StateMan(Command):
    """
    Acknowledgement: Jog
    Assignment of bits:
    Bit 0: Error LIMP
    Bit 1: Error LIMN
    Bit 2: Error HW_STOP
    Bit 3: Error REF
    Bit 5: Error SW_LIMP
    Bit 6: Error SW_LIMN
    Bit 7: Error SW_STOP
    Bit 14: manu_end
    Bit 15: manu_err

    """
    command_index = (0x29, 0x02)
    command_name = 'Manual.stateMan'
    canopen_type = data_types.UINT16
    writable = False


class N_slowMan(Command):
    """
    Speed for slow jog
    The maximum speed of rotation is the value of parameter
    Config.n_maxDrv, 15:18.

    """
    command_index = (0x29, 0x04)
    command_name = 'Manual.n_slowMan'
    canopen_type = data_types.UINT16
    unit = 'min-1'
    factory_value = 60
    persistent = True
    writable = True


class N_fastMan(Command):
    """
    Speed for fast jog
    The maxim um speed of rotation is the value of parameter
    Config.n_maxDrv, 15:18.

    """
    command_index = (0x29, 0x05)
    command_name = 'Manual.n_fastMan'
    canopen_type = data_types.UINT16
    unit = 'min-1'
    factory_value = 600
    persistent = True
    writable = True


class Step_Man(Command):
    """
    Jogging distance at jog start
    Value 0: Direct activation of continuous movement

    """
    command_index = (0x29, 0x07)
    command_name = 'Manual.step_Man'
    canopen_type = data_types.UINT16
    unit = 'Inc'
    factory_value = 20
    persistent = True
    writable = True


class Time_Man(Command):
    """
    Waiting time until continuous movement starts
    Only eff ect iv e if jog di stance is not set to e qual 0 .

    """
    command_index = (0x29, 0x08)
    command_name = 'Manual.time_Man'
    canopen_type = data_types.UINT16
    valid_range = range(1, 10001)
    unit = 'ms'
    factory_value = 500
    persistent = True
    writable = True
