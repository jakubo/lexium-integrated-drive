"""Module autogenerated, may contain errors
Use at your own risk
"""
from afi import data_types
from . import Command


class KPn(Command):
    """
    Speed controller P term
    Unit: [0 0001 A/ . m n-i 1]

    """
    command_index = (0x0F, 0x08)
    command_name = 'Control.KPn'
    canopen_type = data_types.UINT16
    valid_range = range(0, 32768)
    unit = 'A/min-1'
    persistent = True
    writable = True


class TNn(Command):
    """
    Speed controller integral action time
    Unit: [0 0 . 1 ms]

    """
    command_index = (0x0F, 0x09)
    command_name = 'Control.TNn'
    canopen_type = data_types.UINT16
    valid_range = range(100, 32768)
    unit = 'ms'
    persistent = True
    writable = True


class KPp(Command):
    """
    Position controller P term
    Unit: [0 .1 1/s]

    """
    command_index = (0x0F, 0x0A)
    command_name = 'Control.KPp'
    canopen_type = data_types.UINT16
    valid_range = range(0, 1251)
    unit = '1/s'
    persistent = True
    writable = True


class KFPp(Command):
    """
    Speed feed-forward control position controller
    Value 32767: 100% compen sat oni

    """
    command_index = (0x0F, 0x0B)
    command_name = 'Control.KFPp'
    canopen_type = data_types.UINT16
    valid_range = range(0, 32768)
    factory_value = 32767
    persistent = True
    writable = True


class PscDamp(Command):
    """
    Posicast filter for speed controller: attenuation

    """
    command_index = (0x0F, 0x14)
    command_name = 'Control.pscDamp'
    canopen_type = data_types.UINT16
    valid_range = range(51, 101)
    unit = '%'
    factory_value = 100
    persistent = True
    writable = True


class PscDelay(Command):
    """
    Posicast filter for speed controller: delay
    Value 0: Posicast inactiv e
    Unit: [0.1 ms]

    """
    command_index = (0x0F, 0x15)
    command_name = 'Control.pscDelay'
    canopen_type = data_types.UINT16
    valid_range = range(0, 321)
    unit = 'ms'
    factory_value = 0
    persistent = True
    writable = True
