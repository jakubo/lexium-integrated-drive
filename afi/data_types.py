"""Data types and related utils.

There is a little bit verbosity and boilerplate going on.
This is by design god damnit!
"""
import struct

from . import exceptions

# Data is transmitted in little-endian
# as determined by experiment
ENDIAN_FORMAT = '<'


# Maping between Schneiders CANOpen types and formats used
# to decode them into python types
DATA_TYPE_STRUCT_FORMAT_MAP = {
    'BOOLEAN': '?',
    'INT8': 'b',
    'UINT8': 'B',
    'INT16': 'h',
    'UINT16': 'H',
    'INT32': 'i',
    'UINT32': 'I'
}


STRING_TYPES = ('STRING8', 'STRING16')
PACKED_TYPES = DATA_TYPE_STRUCT_FORMAT_MAP.keys()


def _pack_fmt(canopen_type):
    """Returns format for packing/unpacking of ``canopen_type``"""
    fmt_char = DATA_TYPE_STRUCT_FORMAT_MAP[canopen_type]
    return '{}{}'.format(ENDIAN_FORMAT, fmt_char)


def to_python(canopen_type, value):
    """Takes ``value`` of ``canopen_type`` and returns python value"""
    if canopen_type in PACKED_TYPES:
        fmt = _pack_fmt(canopen_type)
        return struct.unpack(fmt, value)[0]
    if canopen_type in STRING_TYPES:
        return value.decode().strip()

    raise exceptions.UnknownType('Unknown type {}'.format(canopen_type))


def to_canopen(canopen_type, value):
    """Returns ``value`` that can be transmitted to canopen node
    that expects ``canopen_type``.
    """
    if value is None:
        return None
    if canopen_type in PACKED_TYPES:
        fmt = _pack_fmt(canopen_type)
        return struct.pack(fmt, value)
    if canopen_type in STRING_TYPES:
        return value.encode()


class _CanopenType:
    """Shortcut for canopen types conversion"""
    canopen_type = None

    def __init__(self, value):
        if isinstance(value, _CanopenType):
            self._canopen = value._canopen
            self._python = value._python
        elif isinstance(value, bytes):
            self._canopen = value
            self._python = to_python(self.canopen_type, value)
        else:
            self._python = value
            self._canopen = to_canopen(self.canopen_type, value)

    def __str__(self):
        return '{}({})'.format(self.canopen_type, self._python)

    def __repr__(self):
        return str(self)

    def __eq__(self, value):
        if isinstance(value, bytes):
            return self._canopen == value
        elif isinstance(value, _CanopenType):
            return self._python == value._python
        return self._python == value

    @property
    def as_python(self):
        """Returns native python value"""
        return self._python

    @property
    def as_canopen(self):
        """Returns canopen bytes"""
        return self._canopen

    @property
    def active_bits(self):
        """Returns bits that are set"""
        if self.canopen_type in STRING_TYPES:
            raise TypeError('Cannot test bits of a string')
        if self._python is None:
            return ()
        from_lsb = bin(self.as_python).replace('0b', '')[::-1]
        return tuple(idx for idx, v in enumerate(from_lsb) if v == '1')


class BOOLEAN(_CanopenType):
    """Boolean canopen type"""
    canopen_type = 'BOOLEAN'


class INT8(_CanopenType):
    """8bit integer"""
    canopen_type = 'INT8'


class UINT8(_CanopenType):
    """8 bit unsigned integer"""
    canopen_type = 'UINT8'


class INT16(_CanopenType):
    """16 bit integer"""
    canopen_type = 'INT16'


class UINT16(_CanopenType):
    """16 bit unsigned integer"""
    canopen_type = 'UINT16'


class INT32(_CanopenType):
    """32 bit integer"""
    canopen_type = 'INT32'


class UINT32(_CanopenType):
    """32 bit unsigned integer"""
    canopen_type = 'UINT32'


class STRING8(_CanopenType):
    """8 character(byte) string"""
    canopen_type = 'STRING8'


class STRING16(_CanopenType):
    """16 character(byte) string"""
    canopen_type = 'STRING16'


def bit(nth, val=1):
    """Returns bitmask (for OR'ing) for nth bit.

    Example bit(3) | bit(1) -> 0b1010
    """
    if not val:
        return 0
    return 1 << nth
