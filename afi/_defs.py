"""Constants etc"""

__all__ = ['JOG_ERROR_BITS']


JOG_POLL_INTERVAL = 0.1

JOG_ERROR_BITS = {
    1: 'LIMP Error',
    2: 'LIMN Error',
    3: 'HW_STOP Error',
    4: 'REF Error',
    5: 'SW LIMP Error',
    6: 'SW LIMN Error',
    7: 'SW Stop Error',
    14: 'Manu stop',
    15: 'Manu error'
}

POWER_STAGE_STARTUP = 0.25
