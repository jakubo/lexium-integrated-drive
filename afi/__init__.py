"""Library for controling ILA1F (Schneider's integrated drive).

Name AFI comes from the fact that the device used for testing when
asked for "manufacturer device name" (at 0x1008) returned " AFI".

Library requires canopen python library.
"""
from importlib import import_module

from . import ctrl
from .exceptions import InvalidCommand
from ._defs import JOG_ERROR_BITS, JOG_POLL_INTERVAL, POWER_STAGE_STARTUP

class Afi:
    """Turn generic ``canopen.Node`` into Lexium abstraction"""
    def __init__(self, node):
        self.node = node

    def __getattr__(self, attr):
        """Python voo-doo for seamless access to all commands"""
        return _CommandProxy(attr, self.node)


class _CommandProxy:
    """Allow dynamic access of commands in ``ctrl`` package"""
    def __init__(self, command_group, node):
        grp = command_group.lower()
        if grp not in ctrl.__all__:
            raise InvalidCommand(command_group)
        self.module = import_module('afi.ctrl.{}'.format(grp))
        self.node = node

    def __getattr__(self, attr):
        try_names = (attr, attr[0].upper() + attr[1:])
        for name in try_names:
            try:
                cmd = getattr(self.module, name)
                return cmd(self.node).read()
            except AttributeError:
                pass

        raise AttributeError('Cannot read function {}'.format(attr))

    def __setattr__(self, attr, val):
        super().__setattr__(attr, val)

        try_names = (attr, attr[0].upper() + attr[1:])
        for name in try_names:
            try:
                cmd = getattr(self.module, name)
                return cmd(self.node).write(val)
            except AttributeError:
                pass
