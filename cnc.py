import operator
from time import sleep
from threading import Thread
from collections import namedtuple, OrderedDict

import canopen

from afi import Afi, JOG_ERROR_BITS, JOG_POLL_INTERVAL, POWER_STAGE_STARTUP
from afi.data_types import bit

# network parameters
net_config = {
    'channel': 'can0',
    'bustype': 'socketcan',
    'bitrate': 500e3
}

# nodes
node_config = OrderedDict((
    ('x', {'addr': 1, 'range_cm': 120, 'range_enc': 2839074.6447, 'home_cw': True}),
    ('y', {'addr': 2, 'range_cm': 120, 'range_enc': 1872763.419, 'home_cw': True}),
    ('z', {'addr': 3, 'range_cm': 39.5, 'range_enc': 615319.5656, 'home_cw': False}),
))

LimitSwitch = namedtuple('LimitSwitch', ('LIMP', 'LIMN'))


class CNCError(Exception):
    """Base class for CNC errors"""


class JogError(CNCError):
    """Raised if cannot jog"""


class CNCNode:
    """CNC node.
    Controls single axis of cnc machine."""
    MAX_RETRY = 3

    def __init__(
            self, canopen_node,
            range_cm=None, range_enc=None,
            home_cw=True, multirun_enc=False):
        self.afi = Afi(canopen_node)
        self.range_cm = range_cm
        self.range_enc = range_enc
        self.home_cw = home_cw
        self.multirun_enc = multirun_enc

    def _cm_to_pos(self, cms):
        """Return  relative position required to move by ``cms``"""
        one_cm = self.range_enc // self.range_cm
        return int(one_cm * cms)

    @property
    def abs_pos(self):
        """Returns absolute position of a node"""
        return self.afi.PTP.p_absPTP

    def limit_switch_status(self):
        """Returns status of limit switches"""
        limp_on, limn_on = self.limit_switch_active()
        io_act = self.afi.IO.IO_act.active_bits
        limp = (0 in io_act) if limp_on else 'x'
        limn = (1 in io_act) if limn_on else 'x'
        return LimitSwitch(limp, limn)

    def limit_switch_active(self):
        """Returns definitions of limit switches"""
        limp = self.afi.IO.IO0_def == 1
        limn = self.afi.IO.IO1_def == 2
        return LimitSwitch(limp, limn)

    def jog_start(self, slow=True, clockwise=True):
        """Start jog.
        Raise JogError if cannot jog.
        """
        jog = bit(3) | bit(2, not slow) | bit(int(not clockwise))
        try:
            self.afi.Manual.startMan = jog
        except canopen.SdoAbortedError as sde:
            self.jog_status()

    def jog_status(self):
        """Returns True if jogging, raises JogError otherwise."""
        status = self.afi.Manual.stateMan
        if status == 0:
            return True
        err_bits = status.active_bits
        errors = (e for b, e in JOG_ERROR_BITS.items() if b in err_bits)
        error_msg = ', '.join(errors)
        raise JogError(error_msg)

    def jog_stop(self):
        """Stop jog"""
        self.afi.Manual.startMan = 0

    def jog_home(self):
        """Jog home.
        Jog fast until limit switch is triggered then slowly move away and
        stop. Set established position as 0.
        This is blocking operation
        """
        try:
            self.jog_start(clockwise=self.home_cw, slow=False)
        except JogError:
            pass
        sleep(JOG_POLL_INTERVAL)
        try:
            while self.jog_status():
                sleep(JOG_POLL_INTERVAL)
        except JogError:
            pass
        sleep(JOG_POLL_INTERVAL)
        self._of_the_switch()
        self.reset_pos()

    def _of_the_switch(self):
        """Moves avay from the switch"""
        self.fault_reset()
        status = self.limit_switch_status()
        active = self.limit_switch_active()
        if status == active:
            return
        sleep(JOG_POLL_INTERVAL)
        self.jog_start(clockwise=(status.LIMP==active.LIMP), slow=True)
        wait = True
        while wait:
            sleep(50e-3)
            limp, limn = self.limit_switch_status()
            wait = limp != limn
        self.jog_stop()
        self.power_stage()

    def quick_stop(self):
        """Trigger quick stop"""
        self.afi.Commands.driveCtrl = bit(2)

    def fault_reset(self):
        """Execute ``fault reset``"""
        self.afi.Commands.driveCtrl = bit(3)

    def power_stage(self, enable=True):
        """Enable or disable power stage"""
        self.afi.Commands.driveCtrl = bit(int(enable))
        if enable:
            sleep(POWER_STAGE_STARTUP)

    def reset_pos(self):
        """Set current position to 0"""
        self.afi.Commands.SetEncPos = 0
        self.afi.Homing.StartSetp = 0

    def _ptp(self, pos_cm, speed=None, mode='rel', retried=0):
        """Move PTP as per ``mode`` (rel or abs)"""
        self.fault_reset()
        self.afi.PTP.v_tarPTP = speed or 60
        direction = -1 if self.home_cw else 1
        cmd = self._cm_to_pos(pos_cm) * direction
        try:
            if mode == 'rel':
                self.afi.PTP.p_relPTP = cmd
            elif mode == 'abs':
                self.afi.PTP.p_absPTP = cmd
            wait = True
            while wait:
                state_bits = self.afi.PTP.StatePTP.active_bits
                wait = not state_bits
                if wait:
                    sleep(50e-3)
                    continue
                if any(b in state_bits for b in(0, 1, 2, 3, 5, 6)):
                    self._of_the_switch()
                    break
        except canopen.SdoAbortedError:
            if retried == self.MAX_RETRY:
                raise
            self.power_stage()
            self._ptp(pos_cm, speed, mode, retried+1)

    def rel_ptp(self, rel_cm, speed=60):
        """Move by ``rel_cm`` at ``speed`` rev/min"""
        self._ptp(rel_cm, speed, 'rel')

    def abs_ptp(self, pos_cm, speed=60):
        """Move to ``pos_cm`` at ``speed`` in rev/min"""
        self._ptp(pos_cm, speed, 'abs')

class CNC:
    """CNC comprised of Schneider ILA1F drives.
    
    This is WIP, no real CNC functionality is yet implemented.
    Only basic manual axis movements.
    """

    def __init__(self, network_config, node_config):
        self.network_config = network_config
        self.node_config = node_config

        self._init_network()
        self._init_nodes()

    def _init_network(self):
        """Initializ and return (can) network object.
        network parameters are expected to be in ``self.network_config``
        """
        network = canopen.Network()
        network.connect(**self.network_config)
        self.network = network
        return network

    def _init_nodes(self):
        """Initialize and return nodes.
        Nodes should be defined in ``self.node_config``"""
        self.nodes = OrderedDict()
        for name, config in self.node_config.items():
            cfg = config.copy()
            addr = cfg.pop('addr')
            canopen_node = self.network.add_node(addr)
            self.nodes[name] = CNCNode(canopen_node, **cfg)

    def _is_jogging(self):
        """Returns True if any of the nodes is jogging"""
        for node in self.nodes.values():
            try:
                return node.jog_status()
            except JogError:
                continue
        return False

    @property
    def abs_pos(self):
        """Returns absolute position of all nodes (cartesian coordinates)"""
        return [x.abs_pos for x in self.nodes.values()]

    def jog_start(self, **nodes):
        """Start jog as per ``nodes``"""
        for node in nodes:
            self.nodes[node].jog_start(**nodes[node])

    def jog_stop(self):
        """Stop all nodes"""
        for node in self.nodes.values():
            try:
                node.jog_stop()
            except Exception as ex:
                print("Couldn't stop {} - {}".format(node, ex))

    def jog_home(self):
        """All nodes go home one by one"""
        self.fault_reset()
        for node in self.nodes.values():
            node.jog_home()

    def quick_stop(self):
        """Quick stop all nodes"""
        for node in self.nodes.values():
            node.quick_stop()

    def fault_reset(self):
        """Execute ``fault reset`` on all nodes"""
        for node in self.nodes.values():
            node.fault_reset()

    def abs_ptp(self, speed=None, **kwargs):
        """Move nodes to provided coordinates, if ``speed`` is provided it
        will be used for all nodes.
        kargs - ``position`` or (``position``, ``node_speed``) tuple.
        node_speed only used if ``speed`` is None
        """
        workers = []
        for node in kwargs:
            try:
                pos, node_speed = kwargs[node]
            except TypeError:
                pos, node_speed = kwargs[node], speed
            worker = Thread(target=self.nodes[node].abs_ptp, args=(pos, node_speed))
            worker.start()
            workers.append(worker)
        for worker in workers:
            worker.join()
